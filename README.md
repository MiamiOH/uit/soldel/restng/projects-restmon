# Get Recorded Data RESTful Web Service

## Description

projects-restmon has been converted into RESTng 2.0 requirements. RESTng 2.0 conversion task focued on fixing syntax or directory structure to meet RESTng 2.0. No functionality or logic change were made. No PHPUnit test code found.
## API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/rest

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`
