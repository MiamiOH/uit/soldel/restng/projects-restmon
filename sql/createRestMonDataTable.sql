BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE rest_mon_data';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE rest_mon_data
   (
    server varchar2(32),
    source varchar2(32),
    seconds decimal,
    method varchar2(12),
    url varchar(1024),
    request_time date
   ) ;

  CREATE INDEX rest_mon_server ON rest_mon_data (server) ;
  CREATE INDEX rest_mon_source ON rest_mon_data (source) ;
  CREATE INDEX rest_mon_time ON rest_mon_data (request_time) ;

