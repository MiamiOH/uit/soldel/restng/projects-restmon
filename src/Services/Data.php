<?php

namespace MiamiOH\ProjectsRestmon\Services;

class Data extends \MiamiOH\RESTng\Service
{
    private $dbDataSourceName = 'RESTMON_DB';
    private $dbh;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function getData()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $data = $this->dbh->queryall_array("
                select server, source, seconds, method, url, 
                    to_char(request_time, 'YYYY-MM-DD HH24:MI:SS') as timeStamp
                    from rest_mon_data
            ");

        $response->setPayload($data);

        return $response;
    }

    public function insertData()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        if (!is_array($data)) {
            throw new \Exception('Payload for ' . __CLASS__ . '::insertData must be an array');
        }

        if (!is_array($data['data'])) {
            throw new \Exception('Payload[data] for ' . __CLASS__ . '::insertData must be an array');
        }

        $records = $data['data'];

        $results = array();
        for ($i = 0; $i < count($records); $i++) {
            try {
                $this->insertDataRecord($records[$i]);
                $results[$i] = array('code' => \MiamiOH\RESTng\App::API_OK, 'message' => '');
            } catch (\Exception $e) {
                $results[$i] = array('code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => $e->getMessage());
            }
        }

        $response->setPayload($results);

        return $response;
    }

    private function insertDataRecord($record)
    {
        $this->dbh->perform("
                insert into rest_mon_data (server, source, seconds, method, url, request_time)
                    values (?, ?, ?, ?, ?, to_date(?, 'YYYY-MM-DD HH24:MI:SS'))
            ", $record['server'], $record['source'], $record['seconds'], $record['method'],
            $record['url'], $record['timeStamp']);
    }

}
