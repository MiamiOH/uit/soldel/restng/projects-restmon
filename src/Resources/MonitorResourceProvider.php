<?php

namespace MiamiOH\ProjectsRestmon\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class MonitorResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'REST\Monitor\Data',
            'class' => 'MiamiOH\ProjectsRestmon\Services\Data',
            'description' => 'Provide methods to insert and retrieve monitor data',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'rest.monitor.v1.read',
            'description' => 'Get recorded data points.',
            'pattern' => '/rest/monitor/v1/data',
            'service' => 'REST\Monitor\Data',
            'method' => 'getData',
            'returnType' => 'collection',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'REST Monitor Service',
                    'module' => 'AccessControl',
                    'key' => 'read'),
            ),
            'options' => array(),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'rest.monitor.v1.create',
            'description' => 'Insert a collection of data points',
            'pattern' => '/rest/monitor/v1/data',
            'service' => 'REST\Monitor\Data',
            'method' => 'insertData',
            'returnType' => 'collection',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'REST Monitor Service',
                    'module' => 'AccessControl',
                    'key' => 'update'),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}