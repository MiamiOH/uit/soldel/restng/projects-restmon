#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

use LWP::UserAgent;
use JSON;
use Date::Parse;
use POSIX qw(strftime);
use Data::Dumper;

# 1. Declare our configuration variables, but do not define them. We
#    will do a defined() test later.
my $logPath;
my $logFileName;
my $keepLogs;
my $dataDir;
my $server;
my $batchSize;
my $username;
my $password;

my $configFile = '';
my $VERBOSE = 0;
my $help = 0;

# 2. Read any options provided on the command line into our configuration
#    variables. This will cause those provided to be defined.
my $result = GetOptions(
  'log_dir=s' => \$logPath,
  'log=s' => \$logFileName,
  'log_history=s' => \$keepLogs,
  'data_dir=s' => \$dataDir,
  'server=s' => \$server,
  'batch=s' => \$batchSize,
  'config=s' => \$configFile,
  'verbose' => \$VERBOSE,
  'help' => \$help
  );

# 3. Document the command line options (and by extension the config file).
if ($help) {
  print <<EOP;

Call notification service for a list of recipients

Options:

           log_dir = path to log file
               log = log file name
       log_history = number of log files to keep
          data_dir = directory containing log files to process
            server = URL to Miami web service
             batch = number of records to post per request[10]
            config = path to config file
           verbose = verbose output
              help = display this help

EOP
}

# 4. Read the config file if provided.
our %config;
if (-e $configFile) {
  open CONFIG, $configFile or die "Couldn't open $configFile: $!";
  while (<CONFIG>) {
    chomp;
    next unless ($_);
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
  }
  close CONFIG;
}

# 5. Set up configuration from the config file, allowing override by
#    command line options if provided.
$logPath = $config{'log_dir'}
  if (!defined($logPath) && exists($config{'log_dir'}));
$logFileName = $config{'log'}
  if (!defined($logFileName) && exists($config{'log'}));
$keepLogs = $config{'log_history'}
  if (!defined($keepLogs) && exists($config{'log_history'}));
$dataDir = $config{'data_dir'}
  if (!defined($dataDir) && exists($config{'data_dir'}));
$server = $config{'server'}
  if (!defined($server) && exists($config{'server'}));
$batchSize = $config{'batch'}
  if (!defined($batchSize) && exists($config{'batch'}));
$username = $config{'username'}
  if (exists($config{'username'}));
$password = $config{'password'}
  if (exists($config{'password'}));

# 6. Apply default configuration for any missing items.
$logPath = '/var/logs' unless (defined($logPath));
$logFileName = 'health_education_log.txt' unless (defined($logFileName));
$keepLogs = 0 unless (defined($keepLogs));
$dataDir = 'data' unless (defined($dataDir));
$server = '' unless (defined($server));
$batchSize = 10 unless (defined($batchSize));
$username = '' unless (defined($username));
$password = '' unless (defined($password));

unless ($server) {
  print "No valid Miami server provided in command line or config file\n";
  exit 1;
}

unless ($username) {
  print "No valid Miami username provided in command line or config file\n";
  exit 1;
}

unless ($password) {
  print "No valid Miami password provided in command line or config file\n";
  exit 1;
}

our $logFile = $logPath . '/' . $logFileName;

# Remove old logs
opendir(LOGDIR, $logPath) or die "Couldn't open $logPath: $!";
while (my $file = readdir(LOGDIR)) {
  next unless ($file =~ /^$logFileName\.(\d+)/);
  unlink($logPath . '/' . $file) if ($1 >= $keepLogs);
}
closedir(LOGDIR);

if ($keepLogs) {
  # Increment exist logs
  for (my $i = $keepLogs; $i >= 1; $i--) {
    my $newNumber = $i + 1;
    rename($logFile . '.' . $i, $logFile . '.' . $newNumber);
  }

  # Archive the most recent log file
  if (-e $logFile) {
    rename($logFile, $logFile . '.1');
  }
}

our $token = '';
our $tokenLastUpdate = 0;

checkMUAuthenticationToken();

my $ua = LWP::UserAgent->new();

# Each directory represents a server. Log files are in the directory.
my @servers;
opendir(my $dh, $dataDir) || die;
while(readdir $dh) {
    next if (/^\./);
    push(@servers, $_);
}
closedir $dh;

# now loop over each directory
foreach my $server (@servers) {
  my $serverDir = $dataDir . '/' . $server;

  print "Reading from $serverDir\n" if ($VERBOSE);

  my @logFiles;
  opendir(my $dh, $serverDir) || die;
  while(readdir $dh) {
      next if (/^\./);
      push(@logFiles, $_);
  }
  closedir $dh;

  my @batch;
  foreach my $logFile (@logFiles) {
    print "Log file $serverDir/$logFile\n" if ($VERBOSE);

    open LOG, "$serverDir/$logFile" or die "Couldn't open $serverDir/$logFile: $!";

    while (<LOG>) {
      chomp;
      
      if (m/ (\d+) - - \[([^\]]+)\] "([^ ]+) (.+) HTTP/) {
        my $seconds = $1;
        my $method = $3;
        my $url = $4;

        my $date = strftime('%Y-%m-%d %H:%M:%S', localtime(str2time($2)));
        
        push(@batch, {
            'server' => $server,
            'source' => 'log',
            'seconds' => $seconds,
            'method' => $method,
            'url' => $url,
            'timeStamp' => $date
          }
        );
      }

      if (scalar(@batch) >= $batchSize) {
        sendBatch('batch' => \@batch);
        @batch = ();
        exit;
      }
    }
  }

  # Catch any left over.
  if (scalar(@batch) >= $batchSize) {
    sendBatch('batch' => \@batch);
    @batch = ();
  }
}

sub sendBatch {
  my $params = { @_ };

  my $batch = $params->{'batch'};

  my $request = HTTP::Request->new('POST', $server . '/rest/monitor/v1/data?token=' . $token);
  my $data = {
      'data' => $batch
  };

  $request->content(to_json($data));
  $request->header('Content-Type', 'application/json');

  my $response = $ua->simple_request($request);

  unless ($response->code == 200) {
    exitWithError('message' => 'Log record insert failed', 'request' => $request,
      'response' => $response);
  }

  print "Send batch of " . scalar(@{$batch}) . "\n" if ($VERBOSE);
  
  return 1;

}

sub extractResponseData {
  my $params = { @_ };

  my $request = $params->{'request'};
  my $response = $params->{'response'};
  my $exitOnError = defined($params->{'exitOnError'}) ? $params->{'exitOnError'} : 1;

  unless ($response->code == 200) {
    my $error = 'Request for service failed';

    $error = 'Authentication failed' if ($response->code == 401);

    if ($exitOnError) {
      exitWithError('message' => $error, 'request' => $request, 'response' => $response);
    } else {
      logEntry('message' => $error, 'request' => $request, 'response' => $response);
    }
  }

  my $payload = {};

  eval(q#$payload = from_json(${$response->content_ref()})#);

  if ($@) {
    exitWithError('message' => 'Failed to parse response ' . $@, 'request' => $request, 'response' => $response);
  }

  unless (defined($payload->{'data'})) {
    exitWithError('message' => 'Profile response contains no data element', 'request' => $request, 'response' => $response);
  }

  return $payload->{'data'};
}

sub exitWithError {
  my $params = { @_ };

  my $message = $params->{'message'} || 'An unknown error occurred';

  print "$message\n";
  print $params->{'request'}->as_string() if ($params->{'request'});
  print $params->{'response'}->as_string() if ($params->{'response'});
  logEntry('message' => $message);
  logEntry('message' => $params->{'request'}->as_string()) if ($params->{'request'});
  logEntry('message' => $params->{'response'}->as_string()) if ($params->{'response'});
  exit 1;
}

sub logEntry {
  my $params = { @_ };

  my $message = $params->{'message'} || '';

  if ($message) {
    my $time = scalar localtime();
    print "[$time] $message\n" if ($VERBOSE);
    open LOG, ">>$logFile" or die "Couldn't open $logFile: $!";
    print LOG "[$time] $message\n";
    close LOG;
  }
}

sub checkMUAuthenticationToken {
  if ($token && time() < $tokenLastUpdate + (55 * 60)) {
    return 1;
  }

  getMUAuthenticationToken();
}

sub getMUAuthenticationToken {
  my $ua = LWP::UserAgent->new();

  my $request = HTTP::Request->new('POST', $server . '/authentication/v1');
  my $data = {
      'username' => $config{'username'},
      'password' => $config{'password'},
      'type' => 'usernamePassword'
  };

  $request->content(to_json($data));
  $request->header('Content-Type', 'application/json');

  my $response = $ua->simple_request($request);

  # Do not log the request since it has username/password
  my $tokenInfo = extractResponseData('response' => $response);

  $token = $tokenInfo->{'token'};
  $tokenLastUpdate = time();

}

